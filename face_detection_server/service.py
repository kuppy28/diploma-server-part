from face_clustering.server_script import app

if __name__ == "__main__":
    print("* Flask starting server...")
    app.run(
        host='0.0.0.0',
        port='8086',
        debug=True)
