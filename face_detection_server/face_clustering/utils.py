import datetime
import time
import json


class Error(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        log(json.dumps(rv))
        return rv


def try_get_json(request):
    if request.method != "POST":
        raise Error("method is not POST")
    payload = json.loads(request.form["json"])
    #payload = json.loads(request.data.decode("utf-8"))S
    if payload is None or payload == '':
        raise Error('Payload is not JSON')
    return payload


def log(message, level='Info'):
    print('* [%s] ~ %s: %s' % (
        datetime.datetime.fromtimestamp(
            time.time()).strftime('%Y-%m-%d %H:%M:%S'),
        level,
        message))
