from io import BytesIO
from PIL import Image
import numpy as np
from sklearn import preprocessing
from sklearn import metrics
import face_recognition
import base64
import json
import cv2
import os
import pickle

import face_features_extractors as ffe
from clusters_algo import *
from approx_rank_order import approx_rank_order

face_recog = 0
vgg2 = 1
mobilenet = 2
facenet = 3
resnet = 4

dbscan_mode = 0
aro_mode = 1
agglomerative_mode = 2
hdbscan_mode = 3
fcluster_mode = 4
kmeans_mode = 5

def face_detection(request):
    print("start face clustering")
    print('detecting faces')
    payload = request
    # list of tuples which contains (path, face feature)
    features = []
    i = 0
    features_file = 'D://features_mobilenet_gfw_4.txt'
    if not os.path.exists(features_file):
        for path, picture in payload.items():
            image = decode_image(picture)
            #boxes = detect_face(image)
            boxes = []
            face_feature = extract_features(image, boxes, mobilenet)
            features.append((path, face_feature))
            i += 1
        with open(features_file, "wb") as fp:  # Pickling
            pickle.dump(features, fp)
    with open(features_file, "rb") as fp:  # Unpickling
        features = pickle.load(fp)

    print('starting make clusters')
    bestThreshold=0
    bestStatistic, prevStatistic = 0, 0
    #for distanceThreshold in np.linspace(1, 71, dtype=np.int):
    # for distanceThreshold in np.linspace(0.0, 3, 50):
    #     currentStatistic=0
    #     result, y_true, y_pred = make_clusters(features, hdbscan_mode, distanceThreshold)
    #     homogeneity, completeness, v_measure = metrics.homogeneity_completeness_v_measure(y_true, y_pred)
    #     currentStatistic += v_measure
    #     # print(distanceThreshold,currentStatistic)
    #     if currentStatistic > bestStatistic:
    #         bestStatistic = currentStatistic
    #         bestThreshold = distanceThreshold
    #     if currentStatistic < prevStatistic - 0.01:
    #         break
    #     prevStatistic = currentStatistic

    result, y_true, y_pred = make_clusters(features, hdbscan_mode, bestThreshold)
    ari, ami, homogeneity, completeness, v_measure, bcubed_precision, bcubed_recall, bcubed_fmeasure = get_clustering_statistics(y_true, y_pred)
    print(result)
    print("best threshold", bestThreshold)
    print('adjusted_rand_score:', ari)
    print('Adjusted Mutual Information:', ami)
    print('homogeneity/completeness/v-measure:', homogeneity, completeness, v_measure)
    print('BCubed precision/recall/FMeasure:', bcubed_precision, bcubed_recall, bcubed_fmeasure)

#B-cubed
def fscore(p_val, r_val, beta=1.0):
    """Computes the F_{beta}-score of given precision and recall values."""
    return (1.0 + beta**2) * (p_val * r_val / (beta**2 * p_val + r_val))

def mult_precision(el1, el2, cdict, ldict):
    """Computes the multiplicity precision for two elements."""
    return min(len(cdict[el1] & cdict[el2]), len(ldict[el1] & ldict[el2])) \
        / float(len(cdict[el1] & cdict[el2]))

def mult_recall(el1, el2, cdict, ldict):
    """Computes the multiplicity recall for two elements."""
    return min(len(cdict[el1] & cdict[el2]), len(ldict[el1] & ldict[el2])) \
        / float(len(ldict[el1] & ldict[el2]))

def precision(cdict, ldict):
    """Computes overall extended BCubed precision for the C and L dicts."""
    return np.mean([np.mean([mult_precision(el1, el2, cdict, ldict) \
        for el2 in cdict if cdict[el1] & cdict[el2]]) for el1 in cdict])

def recall(cdict, ldict):
    """Computes overall extended BCubed recall for the C and L dicts."""
    return np.mean([np.mean([mult_recall(el1, el2, cdict, ldict) \
        for el2 in cdict if ldict[el1] & ldict[el2]]) for el1 in cdict])

def get_BCubed_set(y_vals):
    dic={}
    for i,y in enumerate (y_vals):
        dic[i]=set([y])
    return dic

def BCubed_stat(y_true, y_pred, beta=1.0):
    cdict=get_BCubed_set(y_true)
    ldict=get_BCubed_set(y_pred)
    p=precision(cdict, ldict)
    r=recall(cdict, ldict)
    f=fscore(p, r, beta)
    return (p,r,f)

def get_clustering_statistics(y_true,y_pred):
    ari=metrics.adjusted_rand_score(y_true, y_pred)
    ami=metrics.adjusted_mutual_info_score(y_true, y_pred)
    homogeneity,completeness,v_measure=metrics.homogeneity_completeness_v_measure(y_true, y_pred)
    bcubed_precision,bcubed_recall,bcubed_fmeasure=BCubed_stat(y_true, y_pred)
    return ari,ami,homogeneity,completeness,v_measure,bcubed_precision,bcubed_recall,bcubed_fmeasure

def try_get_json(request):
    payload = json.loads(request)
    #payload = json.loads(request.data.decode("utf-8"))S
    return payload

def decode_image(byte_string):
    img = Image.open(BytesIO(base64.b64decode(byte_string)))
    img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
    rgb = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)
    return rgb

def detect_face(image):
    boxes = face_recognition.face_locations(image, model="cnn")
    return boxes

def crop_face(image, boxes):
    top, right, bottom, left = boxes[0]
    face_image = image[top:bottom, left:right]
    return face_image
    #pil_image = Image.fromarray(face_image)
    #pil_image.save('out.bmp')

def extract_features(image, boxes, i):
    face_features = []
    if i == face_recog:
        face_features = face_recognition.face_encodings(image, boxes)
    else:
        #face_image = crop_face(image, boxes)
        face_image = image
        if i == vgg2 or i == resnet:
            #print("[INFO] extracting features from image using vgg2")
            face_image = cv2.resize(face_image, (224, 224))
            face_features = np.array([ffe.extract_features_vggface(face_image)])
            face_features = preprocessing.normalize(face_features, norm='l2')
        elif i == mobilenet:
            #print("[INFO] extracting features from image using mobilenet")
            face_features = np.array([ffe.extract_features_mobilenet(face_image)])
            face_features = preprocessing.normalize(face_features, norm='l2')
        elif i == facenet:
            #print("[INFO] extracting features from image using facenet")
            face_features = np.array([ffe.extract_features_facenet(face_image)])
            face_features = preprocessing.normalize(face_features, norm='l2')

    return face_features

def make_clusters(face_features_tuple, mode, distanceThreshold):
    # Extract face features and paths
    face_features = np.array([feature for path, feature in face_features_tuple])
    paths = [path for path, feature in face_features_tuple]
    result = {}
    # cluster the embeddings
    print("[INFO] clustering...")

    encodings = []
    for i in face_features:
        encodings.append(i[0])

    pair_dist = metrics.pairwise_distances(encodings)
    labels = None
    if mode == dbscan_mode:
        # vgg eps=1.3
        # mobilenet eps=1.722
        # inception
        # distance threshold, min_samples
        labels = dbscan(pair_dist, distanceThreshold, 1)
    elif mode == aro_mode:
        # knn, distance threshold
        labels = approx_rank_order(pair_dist, 15, distanceThreshold)
    elif mode == agglomerative_mode:
        # n_cluster, knn
        labels = agglomerative_hierarchy(pair_dist,10, 1)
    elif mode == hdbscan_mode:
        #vgg - 3
        # min_samples > 1
        labels = hdbscan(pair_dist, 3)
    elif mode == kmeans_mode:
        # n_cluster
        labels = kmeans(pair_dist, 11)
    elif mode == fcluster_mode:
        # distance threshold
        labels = fast_hierarchy(pair_dist, distanceThreshold)

    label_enc = preprocessing.LabelEncoder()
    label_enc.fit(dirs)
    y_true = label_enc.transform(dirs)
    y_pred = -np.ones(len(y_true))

    labelIDs = np.unique(labels)
    numUniqueFaces = len(labelIDs)
    print("[INFO] # unique faces: {}".format(numUniqueFaces))

    for labelID in labelIDs:
        print("[INFO] faces for face ID: {}".format(labelID))
        idxs = np.where(labels == labelID)[0]

        paths_to_faces = []
        for i in idxs:
            paths_to_faces.append(paths[i])
            y_pred[i] = labelID - 1

        result[labelID] = paths_to_faces

    return result, y_true, y_pred

# with open("C:\\Users\\akuporos\\Desktop\\output\\kim\\kim.png", "rb") as imageFile:
#     str = base64.b64encode(imageFile.read())
#
# with open("C:\\Users\\akuporos\\Desktop\\output\\morgan\\morgan.png", "rb") as imageFile:
#     str1 = base64.b64encode(imageFile.read())
#
# with open("C:\\Users\\akuporos\\Desktop\\output\\lucy\\lucy.png", "rb") as imageFile:
#     str2 = base64.b64encode(imageFile.read())
#
# with open("C:\\Users\\akuporos\\Desktop\\output\\lucy\\lucy1.png", "rb") as imageFile:
#     str3 = base64.b64encode(imageFile.read())
#
# with open("C:\\Users\\akuporos\\Desktop\\output\\morgan\\morgan1.png", "rb") as imageFile:
#     str4 = base64.b64encode(imageFile.read())
#
# json_ = {"a" : str, "b" : str1, "c" : str2, "d" : str3, "e" : str4}

json2_ = {}
#main_directory = "D:\\output_diploma1"
main_directory = "D:\\GFW\\output\\"
dirs = []
i = 0
for root, subdirs, files in os.walk(main_directory):
     for dir in subdirs:
         directory = os.path.join(main_directory,dir)
         print(directory)
         for root1, subdirs1, files1 in os.walk(directory):
             for dir1 in subdirs1:
                 directory1 = os.path.join(directory, dir1)
                 for filename in os.listdir(directory1):
                     common_dir = dir + "_" + dir1
                     dirs.append(common_dir)
                     t = os.path.join(directory1, filename)
                     with open(t, "rb") as imageFile:
                         image_base64 = base64.b64encode(imageFile.read())
                         json2_["{}-{}".format(common_dir, filename)] = image_base64
         i += 1
         if i == 4:
             break
     if i == 4:
         break

# for root, subdirs, files in os.walk(main_directory):
#      for dir in subdirs:
#          directory = os.path.join(main_directory,dir)
#
#          for filename in os.listdir(directory):
#              dirs.append(dir)
#              t = os.path.join(directory, filename)
#              with open(t, "rb") as imageFile:
#                  image_base64 = base64.b64encode(imageFile.read())
#                  json2_["{}-{}".format(dir, filename)] = image_base64
#              # if i == 25:
#              #     break
#              i += 1


face_detection(json2_)