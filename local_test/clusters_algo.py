import sklearn.cluster as cluster
import multiprocessing as mp

# done
def kmeans(feat, n_clusters, **kwargs):
    kmeans = cluster.KMeans(n_clusters=n_clusters,
                            n_jobs=mp.cpu_count(),
                            random_state=0).fit(feat)
    return kmeans.labels_

# done
def fast_hierarchy(feat, distance, hmethod='single', **kwargs):
    import fastcluster
    import scipy.cluster
    links = fastcluster.linkage_vector(feat,
                                       method=hmethod)
    labels_ = scipy.cluster.hierarchy.fcluster(links,
                                               distance,
                                               criterion='distance')
    return labels_

# done
def agglomerative_hierarchy(feat, n_clusters, knn, **kwargs):
    from sklearn.neighbors import kneighbors_graph
    knn_graph = kneighbors_graph(feat, knn, include_self=n_clusters)
    hierarchy = cluster.AgglomerativeClustering(n_clusters=n_clusters,
                                                connectivity=knn_graph,
                                                linkage='ward',
                                                compute_full_tree=True).fit(feat)
    return hierarchy.labels_

# done
def dbscan(feat, eps, min_samples, **kwargs):
    db = cluster.DBSCAN(eps=eps, min_samples=min_samples,
                        n_jobs=mp.cpu_count()).fit(feat)
    return db.labels_

# done
def hdbscan(feat, min_samples, **kwargs):
    import hdbscan
    db = hdbscan.HDBSCAN(min_cluster_size=min_samples)
    labels_ = db.fit_predict(feat)
    return labels_